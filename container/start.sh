#!/bin/sh
echo Wait for services
sleep 5

echo Prepare application
python3 manage.py migrate
python3 manage.py collectstatic --noinput
python3 -m atracks.seed

echo Starting gunicorn
exec gunicorn atracks.wsgi:application --name atracks --bind 0.0.0.0:8000
