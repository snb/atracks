var v = new Vue({
  el: '#app',
  delimiters: ['${','}'],
  data: {
    loading: false,
    number: '',
    data: [],
  },
  methods: {
    get_info() {
      if (!this.number) {
        this.value = 'Unknown';
        return;
      }
      let api_url = '/number/?format=json';
      this.loading = true;
      api_url += '&n=' + this.number;
      this.$http.get(api_url)
          .then((response) => {
            this.loading = false;
            this.data = []
            for (key in response.data.data) {
              this.data.push({key: key, value: response.data.data[key]})
            }
          })
          .catch((err) => {
            this.loading = false;
            this.data = [{key: 'error', value: err.body.message}]
            console.log(err);
          })
    }
  }
});
