from django.contrib import admin
from atracks.models import Region, Pool

admin.site.register(Region, admin.ModelAdmin)
admin.site.register(Pool, admin.ModelAdmin)
