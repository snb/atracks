from celery.schedules import crontab
from celery.task import periodic_task
from django.conf import settings
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

from atracks.celery_app import app
from atracks.models import Region, Company


@periodic_task(run_every=crontab(hour=4), app=app)
def load_numbers():
    def fill_dictionary(df, model, key_field, name_field):
        # get unique names
        items = df[name_field].unique()

        # find new names
        db_items = [obj.name for obj in model.objects.all()]
        new_items = set(items) - set(db_items)
        objs = [model(name=name) for name in new_items]

        # create records with new names
        model.objects.bulk_create(objs, 100)  # 100 is a batch size
        d = dict((obj.name, obj.id) for obj in model.objects.all())  # read against with IDs
        pool[key_field] = pool[name_field].map(d)

        # remove "name" field
        del pool[name_field]

    # read data from CSV and concatenate to single dataframe
    data_frames = [pd.read_csv(url, sep=';', header=None, usecols=[0, 1, 2, 4, 5],
                               names=['prefix', 'start', 'end', 'company_name', 'region_name'])
                   for url in settings.POOL_URLS]
    pool = pd.concat(data_frames)

    # add company and operator missed records
    fill_dictionary(pool, Region, 'region_id', 'region_name')
    fill_dictionary(pool, Company, 'company_id', 'company_name')

    # set indexes for better performance
    pool.set_index(['prefix', 'start', 'end'], inplace=True)

    # connect to DB using SQLAlchemy
    db = settings.DATABASES['default']
    engine = create_engine(URL(drivername='postgres', host=db['HOST'], port=db['PORT'], username=db['USER'],
                               password=db['PASSWORD'], database=db['NAME']))

    # Let's write data to temporary table and apply delete/insert/update queries to main pool table
    with engine.begin() as connection:
        pool.to_sql(name='pools_tmp', con=engine, if_exists='replace', method='multi', chunksize=1000)

        # update existing records
        connection.execute(
            'UPDATE atracks_pool AS p SET region_id = t.region_id, company_id = t.company_id FROM pools_tmp AS t '
            'WHERE (t.company_id <> p.company_id or t.region_id <> p.region_id) AND p.prefix = t.prefix '
            'AND p.start=t.start AND p.end = t.end')

        # insert new records
        connection.execute(
            'INSERT INTO atracks_pool ("prefix", "start", "end", "region_id", "company_id") '
            'SELECT t.prefix, t.start, t.end, t.region_id, t.company_id '
            'FROM public.atracks_pool AS p right join pools_tmp AS t on p.prefix=t.prefix AND p.start = t.start '
            'AND p.end = t.end WHERE p.id IS NULL')

        # delete old records
        connection.execute(
            'DELETE FROM atracks_pool WHERE id IN '
            '(SELECT p.id FROM atracks_pool AS p LEFT JOIN pools_tmp AS t ON p.prefix = t.prefix '
            'AND p.start = t.start AND p.end = t.end where t.start IS NULL)')

        # delete this table
        connection.execute('DROP TABLE pools_tmp')
