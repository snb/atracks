from django.http import JsonResponse

from atracks.models import Pool


def get_number_info(request):
    # parse to prefix and tail
    n = request.GET.get('n')
    country = int(n[0])  # dirty way to get Russian number
    if country != 7:
        return JsonResponse({'result': 'bad', 'message': 'Only russian numbers are supported'}, status=400)
    prefix = int(n[1:4])
    tail = int(n[4:])

    # number record should have this prefix and lay between start and end
    number = Pool.objects.filter(prefix=prefix, start__lte=tail, end__gte=tail)
    if not number:
        return JsonResponse({'result': 'bad', 'message': 'Not found'}, status=400)

    # found number
    number = number.first()
    return JsonResponse({'result': 'success',
                         'data': {'operator': number.company.name, 'region': number.region.name}})
