import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'atracks.settings')

django.setup()

from atracks import tasks

# initial seed of numbers base
if __name__ == '__main__':
    tasks.load_numbers.delay()
