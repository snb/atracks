from django.db import models


class Region(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Company(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Pool(models.Model):
    region = models.ForeignKey(Region, on_delete=models.PROTECT, null=True, blank=True)
    company = models.ForeignKey(Company, on_delete=models.PROTECT, null=True, blank=True)
    prefix = models.IntegerField(db_index=True)
    start = models.IntegerField(db_index=True)
    end = models.IntegerField(db_index=True)

    class Meta:
        ordering = ('prefix', 'start', 'end')

    def __str__(self):
        return '%s %s-(%s-%s)' % (self.region.name, self.prefix, self.start, self.end)
