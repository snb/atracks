# Atracks API

Test application to get cellphone number info

## Install

    docker-compose build

## Run

    docker-compose up runserver

## Usage

`http://0.0.0.0` - main page

`http://0.0.0.0/admin/` admin
